//
//  RESTListViewManager.swift
//  Class_02
//
//  Created by Check Developer on 3/20/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import Foundation

extension URLSession {
    func synchronousDataTask(with url: URL) -> (Data?, URLResponse?, Error?) {
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let dataTask = self.dataTask(with: url) {
            data = $0
            response = $1
            error = $2
            
            semaphore.signal()
        }
        dataTask.resume()
        
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (data, response, error)
    }
}

class RESTListViewManager {

//    func retrieveDataNS(url:String) -> Any?{
//        
//        URLSession.shared.dataTask(with: NSURL(string: url) as! URL) { data, response, error in
//            // Handle result
//            return response
//            }.resume()
//        return nil
//    }
    
    func retrieveData(urlString:String) -> [String:Any] {
        var parsingData:[String:Any] = [:]
        guard let url = URL(string : urlString) else{
            print ("Error parsing url");
            return [:]
        }
        let urlRequest = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        
       
        
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest){
            (data, response, error) in
            guard error == nil else{ //catch no response
                print ("Error communicating with backend")
                print (error!)
                return
            }
            
            guard let responseData = data else{ //catching didnt receive data (backend is responding)
                print ("Error, didnt receive any data")
                return
            }
            
            do{
                guard let jsonData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:Any] else{
                    print ("Error trying to convert data to JSON object")
                    return
                }
                parsingData=jsonData
            }
            catch{
                print ("Error trying to convert data to JSON object")
                return
            }

        }
        task.resume()
        return parsingData
    }
    
    func convertDatatoJSON(data:Data) -> [String:Any]{
        var parsingData:[String:Any] = [:]
        do{
            guard let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else{
                print ("Error trying to convert data to JSON object")
                return [:]
            }
            parsingData=jsonData
        }
        catch{
            print ("Error trying to convert data to JSON object")
            return [:]
        }
        
        return parsingData
    }

}
