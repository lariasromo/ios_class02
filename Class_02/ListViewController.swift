//
//  ListViewController.swift
//  Class_02
//
//  Created by Check Developer on 3/15/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    //var arrLocations = ["Aspen", "Boston", "Charleston", "Chicago", "Houston", "Las Vegas", "Los Angeles", "Miami", "New Orleans", "New York", "Philadelphia", "Portland", "San Antonio", "San Francisco", "Washington District of Columbia"]
    var arrLocations:[String]? = []
    
    func retrieveLocationsREST(urlString:String){
        guard let url = URL(string : urlString) else{
            print ("Error parsing url");
            return
        }
        let urlRequest = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest){
            (data, response, error) in
            guard error == nil else{ //catch no response
                print ("Error communicating with backend")
                print (error!)
                return
            }
            
            guard let responseData = data else{ //catching didnt receive data (backend is responding)
                print ("Error, didnt receive any data")
                return
            }
            
            do{
                guard let jsonData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String] else{
                    print ("Error trying to convert data to JSON object")
                    return
                }
                self.arrLocations=jsonData as [String]?
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                }
            }
            catch{
                print ("Error trying to convert data to JSON object")
                return
            }
            
        }
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString:String = "http://localhost:8123/cities.json"
        self.retrieveLocationsREST(urlString:urlString)

        
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrLocations!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath)
        let city = self.arrLocations?[indexPath[0]]
        cell.textLabel?.text = city
        return cell
    }

}
