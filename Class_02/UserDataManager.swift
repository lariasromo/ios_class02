//
//  UserDataManager.swift
//  Class_02
//
//  Created by Check Developer on 3/14/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import Foundation

class UserDataManager {
fileprivate var items:[UserItem] = []

func fetch() {
    for data in loadData() {
        items.append(UserItem(dict: data))
    }
}

func numberOfItems() -> Int {
    return items.count
}

func explore(at index:IndexPath) -> UserItem {
    return items[index.item]
}

fileprivate func loadData() -> [[String: AnyObject]] {
    guard let path = Bundle.main.path(forResource: "users", ofType: "plist"), let items = NSArray(contentsOfFile: path) else {
        return [[:]]
    }
    
    return items as! [[String : AnyObject]]
}
}
