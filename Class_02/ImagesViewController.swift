//
//  ImagesViewController.swift
//  Class_02
//
//  Created by Check Developer on 3/30/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImagesViewController: UIViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    var detailName:String = ""
    
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet var titleId: UILabel!
    
    
    let image1 = UIImage(named: "american.png")!
    let image2 = UIImage(named: "bar.png")!
    let image3 = UIImage(named: "brewery.png")!
    let image4 = UIImage(named: "burgers.png")!
    let image5 = UIImage(named: "bistro.png")!
    let image6 = UIImage(named: "mexican.png")!
    
    var localSource : [ImageSource] = []
    var selectedImage : UIImage?
    
    //var imageArr : [UIImage] = [image1, image2, image3, image4, image5, image6]
    var imageArr_A : [ImageSource] = []
    var imageArr_B : [ImageSource] = []
    
    var cellValue : String = ""
    
    var images: [UIImage] = []
    var imageSource: [ImageSource] = []
    var urls: [URL] = []
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImages() {
        print("Download Started")
        for url in self.urls {
            getDataFromUrl(url: url) { (data, response, error)  in
                guard let data = data, error == nil else { return }
                print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
                DispatchQueue.main.async() { () -> Void in
                    self.imageSource.append( ImageSource(image: UIImage(data: data)! ) )
                    self.loadSlideshow()
                }
            }
        }
        
    }
    
    func loadSlideshow(){
        if self.imageSource.count == self.urls.count {
            //self.imageSource = [ImageSource(image: image1), ImageSource(image: image2), ImageSource(image: image3), ImageSource(image: image4), ImageSource(image: image5)]
            slideshow.backgroundColor = UIColor.white
            slideshow.slideshowInterval = 5.0
            slideshow.pageControlPosition = PageControlPosition.underScrollView
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
            slideshow.pageControl.pageIndicatorTintColor = UIColor.black
            slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
            slideshow.currentPageChanged = { page in
                print("current page:", page)
            }
            
            // try out other sources such as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
            slideshow.setImageInputs(self.imageSource)
            
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(ImagesViewController.didTap))
            slideshow.addGestureRecognizer(recognizer)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageArr_A = [ImageSource(image: image1), ImageSource(image: image2), ImageSource(image: image3)]
        imageArr_B = [ImageSource(image: image4), ImageSource(image: image5), ImageSource(image: image6)]
        
        
        localSource = [ImageSource(image : selectedImage!)]
        
        slideshow.backgroundColor = UIColor.white
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.underScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        slideshow.pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.urls.append(URL(string: "https://www.industrysafe.com/images/contruction_worker_more_resources.png")!)
        self.urls.append(URL(string: "https://upload.wikimedia.org/wikipedia/commons/f/f8/Grape_worker.jpg")!)
        self.urls.append(URL(string: "http://ewic.org/wp-content/themes/ewic/images/Construction%20Worker.png")!)
        self.urls.append(URL(string: "http://scotthallremodeling.com/wp-content/uploads/2016/01/construction-worker-blueprints.jpg")!)
        self.downloadImages()
        titleId.text = detailName
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ImagesViewController.didTap))
        slideshow.addGestureRecognizer(recognizer)
    
        print(cellValue);
    }
    
    func didTap() {
        slideshow.presentFullScreenController(from: self)
    }
    
    
}
