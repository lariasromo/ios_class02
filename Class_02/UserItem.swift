//
//  UserItem.swift
//  Class_02
//
//  Created by Check Developer on 3/14/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import Foundation


struct UserItem {
    var name:String?
    var image:String?
}

extension UserItem {
    init(dict:[String:AnyObject]) {
        self.name  = dict["name"] as? String
        self.image = dict["image"] as? String
    }
}
