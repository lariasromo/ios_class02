//
//  UserCell.swift
//  Class_02
//
//  Created by Check Developer on 3/14/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var imgExplore:UIImageView!
    
}
