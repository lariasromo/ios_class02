
///
//  MensajesViewController.swift
//  Class_02
//
//  Created by Check Developer on 3/13/17.
//  Copyright © 2017 Universidad Panamericana. All rights reserved.
//

import UIKit

class MensajesViewController: UIViewController, UICollectionViewDataSource {
    let manager = UserDataManager()
    @IBOutlet var collectionView:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello Explore View Controller")
        
        manager.fetch()
        // Do any additional setup after loading the view.
    }
    
//   @IBAction func imageClick(){
//        let myVC = storyboard?.instantiateViewControllerWithIdentifier("detailsViewStory") as! MensajesViewController
//        myVC.titleId = myLabel.text!
//        navigationController?.pushViewController(myVC, animated: true)
//    }
    
    func numberOfSections(in collectionView: UICollectionView) ->Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int)  ->Int{
        return manager.numberOfItems()
    }
    
//    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        assert(sender as? UserCell != nil, "sender is not a collection view")
//        
//        if (self.collectionView?.indexPath(for: sender as! UICollectionViewCell)) != nil {
//            if segue.identifier == "detailsViewStory" {
//                let detailVC: ImagesViewController = segue.destination as! ImagesViewController
//                detailVC.titleId.text = (sender as! UserCell).lblName.text
//                //detailVC.selectedLabel = cellLabels[indexPath.row]
//            }
//        } else {
//            // Error sender is not a cell or cell is not in collectionView.
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            assert(sender as? UserCell != nil, "sender is not a collection view")
            let detailVC: ImagesViewController = segue.destination as! ImagesViewController
            let cell: UserCell! = sender as! UserCell
            detailVC.detailName = cell.lblName.text!
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath as IndexPath) {
            performSegue(withIdentifier: "detailsViewStory", sender: cell)
        } else {
            // Error indexPath is not on screen: this should never happen.
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "testCell", for: indexPath) as! UserCell
        let item = manager.explore(at: indexPath)
        if let name = item.name { cell.lblName.text = name }
        if let image = item.image { cell.imgExplore.image = UIImage(named: image) }

        return cell;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier != nil, segue.identifier == "detailsTransition" {
            let touchCell : UserCell = sender as! UserCell
            let imageController : ImagesViewController = segue.destination as! ImagesViewController
            
            imageController.cellValue = touchCell.lblName.text!
            
            imageController.selectedImage = touchCell.imgExplore.image
        }
        
        
    }
}
